@extends('welcome')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1>About Us</h1>
            <hr>
            </div>
            <div class="col-lg-12">
<p>We are the pioneers in Online training and real-time project assistance services. We provide project oriented training in a manner so that IT personnel can tackle interview challenges and handle different projects with great ease and confidence.</p>

<p>We strive and struggle to complete your job seeking requirements. For this, we have experienced real-time faculties who will offer you tips and tricks about how to apply your learning into your projects and workplace effectively. We are basically committed to provide training on Oracle Fusion Financials, Oracle Fusion SCM(Supply Chain Management) ,Oracle Fusion HCM (Human Capital Management) ,Oracle OAF(Oracle Applications Frame Work) , Oracle ADF (Oracle Applications Development Framework) and JAVA.</p>
<p>We have a proven record of project assistance services and we also have IT personnel placed in leading multinational companies. If you are a professional you can get diverse technology solutions, documentation regarding latest technology and other tips to enhance their skills.</P>
<p>We have corporate experts as trainers for providing implementation and corporate training. We provide training according to the real time environment so that students can have the feel of working on a project implementation. With a proper schedule, we ensure the training program does not stop and students finish the classes in a given time frame.</p>
<p>High-end performers will be provided with assistance for placements. We provide world class training to aspiring persons and concentrate on delivering appropriate skills, suiting the industry. So, with us you will get suitable skills that meet your industry standard.</p>

<p>We struggle hard and make every effort to complete your job hunting requirements. Keeping this in mind, we have experienced and skilled real-time faculties and trainers who will provide you with various tricks and required tip about how you can apply the learning in your projects and in your workplace in an effective way.</p/>
        </div>
    </div>
</div>
@endsection
